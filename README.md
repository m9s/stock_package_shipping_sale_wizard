Master

[![Pipeline status](https://gitlab.com/m9s/stock_package_shipping_sale_wizard/badges/master/pipeline.svg)](https://gitlab.com/m9s/stock_package_shipping_sale_wizard/commits/master)

Develop

[![Pipeline status](https://gitlab.com/m9s/stock_package_shipping_sale_wizard/badges/develop/pipeline.svg)](https://gitlab.com/m9s/stock_package_shipping_sale_wizard/commits/develop)

[![Coverage report](https://gitlab.com/m9s/stock_package_shipping_sale_wizard/badges/develop/coverage.svg)](http://m9s.gitlab.io/stock_package_shipping_sale_wizard)



This module runs with the Tryton application platform.

Installing
----------

See INSTALL

Note
----

This module is developed and tested over a patched Tryton server and
core modules. Maybe some of these patches are required for the module to work.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/stock_package_shipping_sale_wizard/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, forum or IRC channel:

   * http://bugs.tryton.org/
   * http://www.tryton.org/forum
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

